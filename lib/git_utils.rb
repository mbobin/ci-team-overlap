class GitUtils
  def self.get_additions_and_deletions(diff)
    stats = {additions: 0, deletions: 0}

    diff.split("\n").each do |line|
      if line.start_with?('+')
        stats[:additions] += 1
      elsif line.start_with?('-')
        stats[:deletions] += 1
      end
    end

    stats
  end
end
