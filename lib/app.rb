require 'bundler'
Bundler.require(:default)

require 'csv'
require_relative 'config'
require_relative 'git_utils'
require_relative 'merge_requests'

Churn = Struct.new(:file, :additions, :deletions) do
  include Comparable

  def <=>(other)
    other.total_changes <=> total_changes
  end

  def to_csv
    [file, additions, deletions]
  end

  def total_changes
    additions + deletions
  end
end

config = Config.new

if config.project_id.to_s.empty? && config.label.to_s.empty?
  puts "No project or label given"
  exit(1)
end

stats = {}

puts "=== Gathering stats ===\n\n"

puts "Project: #{config.project_id}"
puts "Label: #{config.label}"

puts "\n\n"

MergeRequests.each(config) do |changes|
  changes.each do |change|
    putc '.'
    counts = GitUtils.get_additions_and_deletions(change['diff'])

    stats[change['new_path']] ||= []
    stats[change['new_path']] << counts
  end
end

if stats.empty?
  puts 'No merge requests found for this configuration'
  exit(0)
end

puts "\n\n=== Computing results ===\n\n"

stats = stats.transform_values do |vals|
  vals.inject({additions: 0, deletions: 0}) do |acc, count|
    acc[:additions] += count[:additions]
    acc[:deletions] += count[:deletions]
    acc
  end
end

offset = stats.keys.max_by(&:length).length

stats = stats
  .map { |key, values| Churn.new(key, values[:additions], values[:deletions]) }
  .sort

printf "%-#{offset}s %s %s\n", 'File', 'Additions', 'Deletions'

stats.each do |churn|
  printf "%-#{offset}s %-#9s %s\n", churn.file, churn.additions, churn.deletions
end

FileUtils.mkdir_p('csv')
csv_file = "csv/#{config.project_id}-#{config.label.gsub(/\W+/, '-')}.csv"

CSV.open(csv_file, 'wb', headers: true) do |csv|
  csv << ['label', 'file', 'additions', 'deletions']
  stats.each do |stat|
    csv << [config.label].concat(stat.to_csv)
  end
end
