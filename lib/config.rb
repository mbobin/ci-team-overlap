class Config
  def api_url
    ENV['CI_API_V4_URL']
  end

  def project_id
    ENV['CHURN_PROJECT_ID']
  end

  def label
    ENV['CHURN_LABEL']
  end

  def cache_dir
    "tmp/#{project_id}"
  end

  def sleep_period
    ENV['CHURN_SLEEP'].to_i
  end
end
