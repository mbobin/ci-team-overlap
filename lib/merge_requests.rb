class MergeRequests
  attr_reader :config

  def self.each(config)
    iterator = new(config)
    iterator.iids_by_label do |iids|
      iids.each do |iid|
        yield iterator.changes_for(iid)
      end
    end
  end

  def initialize(config)
    @config = config
  end

  def iids_by_label
    url = "#{api_path}?state=merged&view=simple&sort=asc&labels=#{config.label}"

    loop do
      data, url = get_json(url)
      iids = data.map { |mr| mr['iid'] }
      yield(iids)

      break unless url
    end
  end

  def changes_for(iid)
    cache_file = Pathname(config.cache_dir).join(iid.to_s).tap { |path| FileUtils.mkdir_p(path) }.join('changes.json')
    if cache_file.exist?
      JSON.parse(cache_file.read)
    else
      response, _next = get_json("#{api_path}/#{iid}/changes")
      changes = response['changes']
      cache_file.write(changes.to_json)
      changes
    end
  end

  def get_json(url)
    sleep(config.sleep_period)
    response = HTTP.get(url)
    if response.code != 200
      puts "Got response #{response.code} for #{url}"
      puts response.to_s
      exit(1)
    end

    data = JSON.parse(response.to_s)
    next_page = response.headers['Link'].to_s.match(/.*(\, )?<(.+)>; rel="next"/).to_a[2]

    [data, next_page]
  end

  def api_path
    "#{config.api_url}/projects/#{config.project_id}/merge_requests"
  end
end
